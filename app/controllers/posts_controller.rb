class PostsController < ApplicationController

  def index
    @posts = Post.all.order("created_at desc")
  end

  def author_posts
    @posts = false
    if author_signed_in?
      @posts = current_author.posts.order("created_at desc")
    end
  end

end
