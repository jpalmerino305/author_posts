class Api::CommentsController < ApplicationController

  before_filter :authenticate_author!

  def create
    comment = Comment.new(comment_params)
    if comment.save
      status = 1
    else
      status = 0
    end
    render json: { status: status, comment: comment }
  end

  private

  def comment_params
    params.require(:comment).permit(:post_id, :name, :content).merge(author_id: current_author.id)
  end

end
