class Api::PostsController < ApplicationController

  before_filter :authenticate_author!

  def create
    post = Post.new(post_params)
    if post.save
  
      post_return = {
        id: post.id,
        name: post.name,
        content: post.content,
        created_at: post.created_at,
        first_name: post.author.first_name,
        last_name: post.author.last_name,
      }

      status = 1
    else
      status = 0
    end
    render json: { status: status, post: post_return }
  end

  private

  def post_params
    params.require(:post).permit(:name, :content).merge(author_id: current_author.id)
  end

end
