class Api::RepliesController < ApplicationController

  before_filter :authenticate_author!

  def create
    reply = Reply.new(reply_params)
    if reply.save
      status = 1
    else
      status = 0
    end
    render json: { status: status, reply: reply }
  end

  private

  def reply_params
    params.require(:reply).permit(:comment_id, :name, :content).merge(author_id: current_author.id)
  end

end
