// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .





$(document).on('submit', '.new-comment', function(e){
    e.preventDefault();

    var form = $(this);
    var token = $('meta[name="csrf-token"]');
    var post_id = $('input[name="post_id"]', $(this));
    var content = $('textarea[name="content"]', $(this));

    $.ajax({
      type: 'POST',
      async: false,
      url: '/api/1.0/comments',
      data: {
        authenticity_token: token.attr('content'),
        comment:{
          post_id: post_id.val(),
          content: content.val()
        }
      },
      success: function(result){
        if (result.status == 1){
          var comment = result.comment;
          var html_str = '';

          html_str += '<li>';
            html_str += comment.content;
            html_str += '&nbsp;&nbsp;&nbsp;';
            html_str += '<a href="" class="reply-link">Reply</a>';
            html_str += '<form action="" class="reply-form">';
              html_str += '<input type="hidden" name="comment_id" value="' + comment.id + '">';
              html_str += '<textarea name="content"></textarea>';
              html_str += '<br />';
              html_str += '<button>Submit</button>';
              html_str += '&nbsp;|&nbsp;';
              html_str += '<a href="" class="cancel-link">Cancel</a>';
            html_str += '</form>';
            html_str += '<ul class="sub-comments"></ul>';
          html_str += '</li>';

          form.closest('.post').find('.title').show();
          form.closest('.post').find('.all-comments').prepend(html_str);

        }
        content.val('');
        console.log(result);
      },
      error: function(result){
          $('#post-loader').hide();
      }
    });

    return false;

});


$(document).on('submit', '.reply-form', function(e){
    e.preventDefault();

    var form = $(this);
    var token = $('meta[name="csrf-token"]');
    var comment_id = $('input[name="comment_id"]', $(this));
    var content = $('textarea[name="content"]', $(this));

    console.log('comment_id: ' + comment_id.val());
    console.log('content: ' + content.val());

    $.ajax({
      type: 'POST',
      async: false,
      url: '/api/1.0/replies',
      data: {
        authenticity_token: token.attr('content'),
        reply:{
          comment_id: comment_id.val(),
          content: content.val()
        }
      },
      success: function(result){
        if (result.status == 1){
          var comment = result.comment;
          var html_str = '';

          html_str += '<li>';
            html_str += content.val();
          html_str += '</li>';

          form.hide();
          content.val('');
          form.closest('li').find('.sub-comments').prepend(html_str);

        }
        content.val('');
        console.log(result);
      },
      error: function(result){
          $('#post-loader').hide();
      }
    });

    return false;

});

$(document).on('click', '.reply-link', function(e){
    e.preventDefault();

    var form = $(this).next('.reply-form');
    form.css({ 'display': 'block'});

    console.log(form)

    // $.ajax({
    //   type: 'POST',
    //   async: false,
    //   url: '/api/1.0/comments,
    //   data: form_data,
    //   success: function(result){
    //     if (result.status == 1){
    //       var html_str = '';

    //     }
    //     console.log(result);
    //   },
    //   error: function(result){
    //       $('#post-loader').hide();
    //   }
    // });

    return false;

});

$(document).on('click', '.cancel-link', function(e){
    e.preventDefault();
    var form = $(this).parent('.reply-form');
    form.css({ 'display': 'none'});
    return false;
});


$(document).ready(function(){

  $('#post-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();

    $('#post-loader').html('Saving...').show();

    var form = $(this);
    var authenticity_token = $('input[name="authenticity_token"]');
    var name = $('input[name="name"]');
    var content = $('textarea[name="content"]');

    var errors = validateForm();
    var html_errors = '';

    if (errors.length > 0){
      for (var i = 0; i < errors.length; i ++){
        html_errors += '<li>' + errors[i] + '</li>';
      }
      $('#form-errors').html(html_errors).show();
      $('#post-loader').hide();
    } else {
      console.log('Valid form....');

      var form_data = {
        authenticity_token: authenticity_token.val(),
        post:{
          name: name.val(),
          content: content.val()
        }
      }

      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: form_data,
        success: function(result){
          console.log(result);
          if (result.status == 1){
            // window.location.href = form.attr('redirect-url');
            var post = result.post;
            var html_str = '';

            html_str += '<div class="post">';
              html_str += '<h3>' + post.name + '</h3>';
              html_str += '<p>' + post.content + '</p>';
              html_str += '<span class="timestamp">By: ' + post.first_name + ' ' + post.last_name + '</span>';
              html_str += '<span class="timestamp">Created: ' + post.created_at + '</span>';

              html_str += '<div class="comment-form">';
                html_str += '<form action="" class="new-comment">';
                  html_str += '<h4>New Comment</h4>';
                  html_str += '<input type="hidden" name="post_id" value="' + post.id + '">';
                  html_str += '<textarea name="content"></textarea>';
                  html_str += '<br />';
                  html_str += '<button type="submit">Submit</button>';
                html_str += '</form>';
              html_str += '</div>';

              html_str += '<div class="comments-list">';
                html_str += '<h4 class="title" style="display: none;">Comments</h4>';
                html_str += '<ul class="all-comments">';
                html_str += '</ul>';
              html_str += '</div>';
            html_str += '</div>';

            $('#post-list').prepend(html_str);

            name.val('');
            content.val('');

            $('#post-loader').hide();

          }
        },
        error: function(result){
            $('#post-loader').hide();
        }
      });

    }

    return false;
  });

  function validateForm(){
    var name = $('input[name="name"]');
    var content = $('textarea[name="content"]');
    var errors = [];

    if (name.val() == ''){
      errors.push('Post name can\'t be blank');
    }

    if (content.val() == ''){
      errors.push('Post content can\'t be blank');
    }

    return errors;
  }

});